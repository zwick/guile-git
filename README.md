# guile-git

This is the repository of guile bindings of git hosted
@ [gitlab](https://gitlab.com/guile-git/guile-git):

```bash
git clone https://gitlab.com/guile-git/guile-git.git
```

## Documentation

Read the source and have a look at [libgit2 reference](http://libgit2.github.com/)

## How to contribute

To start hacking on guile-git install [guix](https://gnu.org/s/guix) and run the
following command:

```bash
> guix environment -l guix.scm
```

You can then:

- Create a pull request on gitlab
- Send a patch to one of the maintainer
- Come and ping `amz3` or `OrangeShark` about it at `#guile@irc.freenode.net`.

And don't forget to add a unit test!
